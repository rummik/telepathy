#!/usr/bin/env node
var Telepathy = require('../lib/telepathy');
var fs = require('fs');
var opts = require('optimist');
var json = require('../package.json');

opts = opts.options('c', {
	describe: 'config file',
	alias: 'config',
	default: process.env.HOME + '/.telepathy.json'
});

if (!fs.existsSync(opts.argv.config)) {
	console.error('Creating default config at:', opts.argv.config);
	console.error('Important!  You _must_ add your own secret key to this file!');
	fs.writeFileSync(opts.argv.config, fs.readFileSync(__dirname + '/../example/telepathy.json'));
	process.exit();
}

var config = JSON.parse(fs.readFileSync(opts.argv.config));

if (!config.secret) {
	console.error('Option "secret" missing or empty in', opts.argv.config);
	console.error('Did you remember to edit the file?');
	process.exit();
}

if (typeof config.safe != 'undefined') {
	console.error('Deprecated option "safe" in use in', opts.argv.config);
	console.error('Please use "lax" instead');
	console.error('This option will be removed in later versions');
}

opts = opts.usage('Telepathically manage passwords.\nUsage: $0\nVersion: ' + json.version).options({
	d: {
		alias: 'domain',
		demand: true,
	},
	u: {
		default: config.username || process.env.USER,
		alias: 'username',
	},
	l: {
		describe: 'password length',
		alias: 'length',
		default: config.length || 10,
	},
	n: {
		describe: 'number of passwords to display',
		alias: 'count',
		default: config.count || 5,
	},
	i: {
		describe: 'starting password index',
		alias: 'index',
		default: config.index || 0,
	},
	s: {
		describe: '[deprecated] see lax',
		boolean: true,
		alias: 'safe',
		default: false,
	},
	x: {
		describe: 'lax mode (use base 62 instead of 94)',
		boolean: true,
		alias: 'lax',
		default: (typeof config.lax != 'undefined' ? config.lax : config.safe) || false,
	},
	a: {
		describe: 'hashing algorithm to use',
		alias: 'algorithm',
		default: config.algorithm,
	},
});

var argv = opts.argv;

if (argv.safe) {
	console.error('Deprecated option "-s" or "--safe" used, please use "-x" or "--lax" instead');
}

if (!(argv.algorithm.toUpperCase() in Telepathy.algorithms)) {
	console.error('Invalid algorithm supplied');
}

var telepathy = new Telepathy({
	secret: config.secret,
	length: argv.length,
	user: argv.username,
	domain: argv.domain,
	alphabet: (argv.lax || argv.safe) ? Telepathy.alphabet.base62 : Telepathy.alphabet.base94,
	algorithm: argv.algorithm,
});

for (var i=argv.index; i<argv.index + argv.count; i++)
	console.log(telepathy.password(i));
